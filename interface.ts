
interface Leptop {
    name:string;
    on():void;
    off():void;
    isGaming:boolean;
}

class Asus implements Leptop{
    name: string="asus"
    isGaming:boolean=true
    on():void{
        console.log("nyala 2 menit")
    }
    off(): void {
        console.log("mati 2 menit")
    }


}

class Macbook implements Leptop{

    name: string="macbook"
    isGaming:boolean=false
    on():void{
        console.log("nyala 1 menit")
    }
    off(): void {
        console.log("mati 1 menit")
    }

}

let asus= new Asus();

console.log(asus.name)
asus.on();
asus.off()

let macbook= new Macbook();
console.log(macbook.name)
macbook.on();
macbook.off();


