
class Person {

    public  name:string;
    public age:number;

    constructor(name:string,age:number){
        this.name=name;
        this.age=age;
    }
    public setName=(name:string):void=>{
        this.name=name
    }
    public getName():string{
        return this.name
    }

}


const person= new Person("abdul",22);

console.log(person.name);
console.log(person.age);


// inheretence typescript
class Admin extends Person {
   public read:boolean=true;
   public write:boolean=true;
   public phone:string;
   private _email:string="";
   public static getRoleName:string="Admin";

    constructor(phone:string,name:string,age:number){
        super(name,age)
        this.phone=phone
        
    }

    public getRole():{read:boolean,write:boolean}{
        return {
            read:this.read,
            write:this.write
        }
    }

    set email(value:string){

        // validasi email
        if (value.length<5){
            this._email="panjang email kurang dari 5"
        }else {

            this._email=value

        }

       
    }

    public get email():string{
        return this._email;
    }

    


}

let admin= new Admin("081913520891","abdul",22);

// console.log(admin.getRole());
// console.log(admin.name)
// admin.setName("hamzan");

// admin.email="abdullah.hamzan@gmail.com"

admin.email="abd"

// console.log(admin.getName());
console.log(admin.email)

console.log(Admin.getRoleName)
