
class List <T>{

    private data:T[];

    constructor(...elements:T[]){
        this.data=elements;
    }

    addElement(element:T):void{
        this.data.push(element)
    }

    addMultipleElement(elements:T[]):void{
        this.data.push(...elements);

    }

    getAllElements():T[]{
        return this.data;

    }


    
}

const dataObject= new List<number>(2,4,6,6,6);
// dataObject.addElement(4);
dataObject.addMultipleElement([10,12,15,16]);


console.log(dataObject.getAllElements());

const hasilObject=dataObject.getAllElements();
hasilObject.map((item,index)=>console.log(`index ke-${index} =${item}`));

let randomData= new List<number | string> (2,"abdul",3,"hamzan");

console.log(randomData.getAllElements())

